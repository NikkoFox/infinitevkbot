import json
import logging
import os
import re

import aiohttp

from .. import command_proc
from .. import vkapi
from ..message import bot_path

with open(os.path.join(bot_path, 'commands', 'vinci_ext', 'vinci_filters.json')) as json_data:
    filters = json.load(json_data)


async def vinci(peer_id, msg, redis):
    check_ttl_id = await redis.get(f"vinci:{peer_id}")
    message = msg['object']['text'].lower()
    message = re.sub(r'\[[^\]]*\]', '', message).strip()
    if not check_ttl_id:
        if len(message.split(' ')) < 2:
            temp = []
            for i in range(len(filters)):
                temp.append(f"{i} — {filters[i]['name']}")
            temp = [temp[i:i + 2] for i in range(0, len(temp), 2)]
            temp = [' | '.join(i) for i in temp]
            reply = '\n'.join(temp)
            reply = f'Прикрепи фото и выбери фильтр\n\n' \
                    f'винчи [номер фильтра]\nНапример: винчи 25\n\n' \
                    f'{reply}'
            return reply, ''
        else:
            temp = msg['object']
            img_url = {}
            if not temp['attachments']:
                return 'Видимо ты не прикрепил фото', ''
            if temp['attachments'][0]['type'] == 'photo':
                for size in temp['attachments'][0]['photo']['sizes']:
                    if size['type'] == 'z' or size['type'] == 'y' or size['type'] == 'x' or size['type'] == 'r':
                        img_url[size['type']] = size['url']
            if 'z' in img_url:
                image_url = img_url.get('z')
            elif 'y' in img_url:
                image_url = img_url.get('y')
            elif 'x' in img_url:
                image_url = img_url.get('x')
            elif 'r' in img_url:
                image_url = img_url.get('r')
            else:
                return 'Ошибка. Попробуй другое изображение', ''
            async with aiohttp.ClientSession() as session:
                async with session.get(image_url) as resp:
                    image = await resp.read()
            if message.split(' ')[1] == 'all' or message.split(' ')[1] == 'все':
                await vkapi.send_message(peer_id, 'Поздравляю, ты нашёл секретную функцию! =)\n'
                                                  'Я добавлю эту команду для тебя в игнор на 3 минуты, а '
                                                  'ты получишь все фильтры для своей фотографии!\n\n'
                                                  'Начинаю свою работу, ожидай)')
                attach = []
                try:
                    k = 0
                    preload = await vinci_preload(image)
                    await redis.setex(f"vinci:{peer_id}", 180, 'True')
                    count = 0
                    while k < len(filters):
                        try:
                            vinci_photo = await vinci_process(filters[k]['id'], preload)
                            image_uploader = vkapi.ImageUploader(peer_id, vinci_photo)
                            attachment = await image_uploader.upload()
                            attach.append(attachment)
                        except:
                            continue
                        if len(attach) == 10 or k == len(filters) - 1:
                            count += 1
                            attach = ','.join(attach)
                            await vkapi.send_message(peer_id, f'{count}-я порция фоточек', attach)
                            attach = []
                        k += 1
                    return 'Дело сделано! =)', ''
                except Exception as e:
                    logging.exception(e)
                    await redis.delete(f"vinci:{peer_id}")
                    return 'Ошибка, попробуй позже', ''
            else:
                try:
                    filter_id = filters[int(message.split(' ')[1])]['id']
                except Exception as e:
                    logging.exception(e)
                    return 'Повтори ввод. Видимо ты ошибся с номером эффекта', ''
                try:
                    preload = await vinci_preload(image)
                    vinci_photo = await vinci_process(filter_id, preload)
                    image_uploader = vkapi.ImageUploader(peer_id, vinci_photo)
                    attach = await image_uploader.upload()
                except Exception as e:
                    logging.exception(e)
                    return 'Ошибка, попробуй позже', ''
                message = f"Вот твоё изображение\nТы использовал фильтр: {message.split(' ')[1]} — " \
                          f"{filters[int(message.split(' ')[1])]['name']}\n"
                await redis.setex(f"vinci:{peer_id}", 10, 'True')
                return message, attach
    else:
        ttl = await redis.ttl(f"vinci:{peer_id}")
        return f'Повтори эту команду через {ttl} сек', ''


async def vinci_process(filter_id, preload):
    async with aiohttp.ClientSession() as session:
        async with session.get(f"http://vinci.camera/process/{preload}/{filter_id}") as resp:
            image = await resp.read()
            return image


async def vinci_preload(content):
    form_data = aiohttp.FormData()
    form_data.add_field('files', content, filename='out.jpg')
    async with aiohttp.ClientSession() as session:
        async with session.post("http://vinci.camera/preload", data=form_data) as resp:
            response = await resp.json()
            return response['preload']


command = command_proc.Command()

command.keys = ['винчи', 'vinci', 'dbyxb', 'винчи все', 'винчи all', 'винчи всё']
command.description = 'Обработаю фотографию с использованием нейронных сетей и фильтров Vinci'
command.settings = 'user/conv'
command.process = vinci
