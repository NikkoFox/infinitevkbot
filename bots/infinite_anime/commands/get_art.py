import logging

from .. import command_proc
from .. import vkapi


async def get_art(*args):
    redis = args[2]
    check_ttl_id = await redis.get(f"get_art:{args[0]}")
    if not check_ttl_id:
        await redis.setex(f"get_art:{args[0]}", 3, 'True')
        try:
            attachment, message = await vkapi.random_picture(redis)
        except Exception as e:
            logging.exception(e)
            attachment, message = '', 'Ошибочка\nПопробуй снова позже'
        return message, attachment
    else:
        ttl = await redis.ttl(f"get_art:{args[0]}")
        return f'Повтори эту команду через {ttl} сек', ''


command = command_proc.Command()

command.keys = ['кинь арт', 'арт', 'рандомный арт']
command.description = 'Пришлю рандомный арт'
command.settings = 'user/conv'
command.process = get_art
