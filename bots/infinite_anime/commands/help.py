from .. import command_proc


async def _help(*args):
    message = 'Список команд:\n'
    if args[0] < 2000000000:
        for c in command_proc.commands_list:
            if c.settings == 'user/conv' or c.settings == 'user':
                message += f'{c.keys[0].title()} — {c.description}\n'
    else:
        for c in command_proc.commands_list:
            if c.settings == 'user/conv' or c.settings == 'conv':
                message += f'{c.keys[0].title()} — {c.description}\n'
    message += f"\nСвязаться с администратором: @id1 (Артём)\n" \
               f"\nПредложить новые команды можно тут: https://vk.cc/866e8K"
    return message, ''


info_command = command_proc.Command()

info_command.keys = ['помощь', 'help']
info_command.description = 'Покажу список команд'
info_command.settings = 'user/conv'
info_command.process = _help
