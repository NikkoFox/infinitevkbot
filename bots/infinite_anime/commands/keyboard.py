import aiosqlite

from .. import command_proc
from .. import vkapi

DB_VK_PATH = vkapi.DB_VK_PATH


async def setting_keyboard(*args):
    redis = args[2]
    check_ttl_id = await redis.get(f"get_key:{args[0]}")
    if int(args[0]) > 2000000000:
        if not check_ttl_id:
            async with aiosqlite.connect(DB_VK_PATH) as connect:
                async with connect.execute(
                        "SELECT show_keyboard FROM keyboard_settings WHERE cid = '%s'" % args[0]) as cursor:
                    show_keyboard = await cursor.fetchone()
                if show_keyboard:
                    show_keyboard = show_keyboard[0]
                await redis.setex(f"get_key:{args[0]}", 3600, 'True')
                if show_keyboard:
                    await connect.execute(
                        "INSERT OR REPLACE INTO keyboard_settings (cid, show_keyboard) VALUES ('%s', '%s')" % (
                            int(args[0]), 0))
                    keyboard = {"one_time": True, "buttons": []}
                    await vkapi.send_custom_keyboard(args[0], 'Кнопки в данной беседе отключены', keyboard=keyboard)
                else:
                    await connect.execute(
                        "INSERT OR REPLACE INTO keyboard_settings (cid, show_keyboard) VALUES ('%s', '%s')" % (
                            int(args[0]), 1))
                    await vkapi.send_message(args[0], 'Кнопки в данной беседе включены', keyboard=True)
                await connect.commit()
            return 'Успешно выполнено!\nИзменять статус кнопок можно раз в час', ''
        else:
            ttl = await redis.ttl(f"get_key:{args[0]}")
            return f'Повтори эту команду через {ttl} сек', ''
    else:
        return 'Команда доступна только в беседах', ''


command = command_proc.Command()

command.keys = ['!управление кнопками']
command.description = 'Управление кнопками в беседе (вкл/выкл)'
command.settings = 'conv'
command.process = setting_keyboard
