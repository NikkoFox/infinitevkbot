import json
import logging
import random

import aiosqlite
from bs4 import BeautifulSoup

from .. import command_proc
from .. import vkapi
from ..shikimoriapi import ShikimoriApi, DB_PATH

DB_VK_PATH = vkapi.DB_VK_PATH


async def create_vk_keyboard(uid, redis):
    check_ttl_id = await redis.get(f"recomm_anime:{uid}")
    if check_ttl_id:
        ttl = await redis.ttl(f"recomm_anime:{uid}")
        return f'Повтори эту команду через {ttl} сек'
    async with aiosqlite.connect(DB_PATH) as connect:
        async with connect.execute("SELECT * FROM genres") as cursor:
            genres = await cursor.fetchall()
    keyboard = [{"action": {"type": "text", "label": f"{x[2]}",
                            "payload": "{\"genre\": \"%d-%s\"}" % (x[0], x[1])}, "color": "default"} for x in genres]
    keyboard.append({"action": {"type": "text", "label": f"Назад",
                                "payload": "{\"genre\": \"Вернуться назад\"}"}, "color": "positive"})
    keyboard = zip(*[iter(keyboard)] * 4)
    return list(keyboard)


async def get_anime(data, redis):
    uid = data['peer_id']
    try:
        shikiapi = ShikimoriApi()
        await shikiapi.create_headers()
        genre = json.loads(data['payload'])['genre']
        if genre != 'Вернуться назад':
            async with aiosqlite.connect(DB_PATH) as connect:
                async with connect.execute(
                        "SELECT max_page FROM genres WHERE id = '%s'" % genre.split('-')[0]) as cursor:
                    max_page = await cursor.fetchone()
            pages = list(range(1, max_page[0]))
            random.shuffle(pages)
            for i in pages:
                params = {'score': '7', 'order': 'popularity', 'genre': genre, 'limit': 1, 'page': i}
                anime = await shikiapi.get_anime(params)
                if anime:
                    break
            await redis.setex(f"recomm_anime:{uid}", 10, 'True')
            anime_info = await shikiapi.get_info_anime(anime[0]['id'])
            genres = ', '.join([x['russian'] for x in anime_info['genres']])
            soup = BeautifulSoup(
                anime_info['description_html'].replace('<div class="b-prgrph">', '\n').replace('<br>', '\n'),
                'lxml')
            try:
                description = soup.get_text()
            except:
                description = ''
            message = f"{anime_info['name']} / {anime_info['russian']}"
            message = f"{message}\n\n" \
                      f"Жанры: {genres}\n" \
                      f"Рейтинг: {anime_info['score']}\n" \
                      f"\n{description}\n\n" \
                      f"Подробнее на shikimori: https://shikimori.one{anime_info['url']}" \
                      f"\nПодробнее на MyAnimeList: https://myanimelist.net/anime/{anime_info['id']}"
            return message, ''

        else:
            return 'Ты вернулся назад', ''
    except Exception as e:
        logging.exception(e)
        return "Возникла какая-то ошибка\nПопробуй еще раз позже", ''


async def send_genres_keyboard(*args):
    try:
        user_id = args[0]
        redis = args[2]
        keyboard = await create_vk_keyboard(user_id, redis)
        if isinstance(keyboard, list):
            keyboard = {"one_time": True, "buttons": keyboard}
            await vkapi.send_custom_keyboard(user_id, 'Выбери жанр', keyboard)
            return '', ''
        else:
            return keyboard, ''
    except Exception as e:
        logging.exception(e)
        return "Возникла какая-то ошибка\nПопробуй еще раз позже", ''


command = command_proc.Command()

command.keys = ['что посмотреть', 'рекомендация', 'посоветуй аниме']
command.description = 'Посоветую годное аниме'
command.settings = 'user'
command.process = send_genres_keyboard
