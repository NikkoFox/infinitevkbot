from .. import command_proc
from .. import vkapi


async def get_gif(*args):
    redis = args[2]
    check_ttl_id = await redis.get(f"get_gif:{args[0]}")
    if not check_ttl_id:
        await redis.setex(f"get_gif:{args[0]}", 5, 'True')
        message, attachment = await vkapi.random_wall_gif(redis)
        return message, attachment
    else:
        ttl = await redis.ttl(f"get_gif:{args[0]}")
        return f'Повтори эту команду через {ttl} сек', ''


command = command_proc.Command()

command.keys = ['кинь гифку', 'гифка', 'гиф']
command.description = 'Пришлю рандомную гифку'
command.settings = 'user/conv'
command.process = get_gif
