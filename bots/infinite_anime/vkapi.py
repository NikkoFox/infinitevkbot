import asyncio
import json
import logging
import os
import random
import time

import aiofiles
import aiohttp

import config

DB_VK_PATH = os.path.join(os.path.split(os.path.relpath(__file__))[0], "db", "vk.sqlite")


async def send_message(peer_id, message, attachment="", keyboard=False):
    try:
        if keyboard:
            if int(peer_id) < 2000000000:
                keyboard = {"buttons": [[{"action": {"type": "text", "label": "Кинь арт"},
                                          "color": "default"},
                                         {"action": {"type": "text", "label": "Кинь гифку"},
                                          "color": "default"}],
                                        [{"action": {"type": "text", "label": "Винчи"},
                                          "color": "default"},
                                         {"action": {"type": "text", "label": "Откуда"},
                                          "color": "default"}
                                         ],
                                        [{"action": {"type": "text", "label": "Посоветуй аниме"},
                                          "color": "positive"}],
                                        [{"action": {"type": "text", "label": "Помощь"},
                                          "color": "primary"}]
                                        ]}
            else:
                keyboard = {"buttons": [[{"action": {"type": "text", "label": "Кинь арт"},
                                          "color": "default"},
                                         {"action": {"type": "text", "label": "Кинь гифку"},
                                          "color": "default"}],
                                        [{"action": {"type": "text", "label": "Винчи"},
                                          "color": "default"},
                                         {"action": {"type": "text", "label": "Откуда"},
                                          "color": "default"},
                                         {"action": {"type": "text", "label": "Помощь"},
                                          "color": "primary"}]
                                        ]}
            params = {"access_token": config.token, "peer_id": str(peer_id), "message": message,
                      "attachment": attachment, "keyboard": json.dumps(keyboard, ensure_ascii=False),
                      "dont_parse_links": 1, 'v': '5.131', 'random_id': int(time.time() * 100000)}
        else:
            params = {"access_token": config.token, "peer_id": str(peer_id), "message": message,
                      "attachment": attachment, 'v': '5.131', 'random_id': int(time.time() * 100000)}
        start_time = time.time()
        async with aiohttp.ClientSession() as __session:
            async with __session.post("https://api.vk.com/method/messages.send", data=params) as resp:
                logging.debug('messages.send: {}, {}, {}'.format(resp.status, resp.method, resp.headers))
                resp = await resp.json()
        logging.debug(f"------|SEND MESSAGE: {resp.get('response')}|TIME:{time.time() - start_time}s------")
    except Exception as e:
        logging.exception(e)
        params = {"access_token": config.token, "peer_id": str(peer_id),
                  "message": 'Ошибка, попробуй позже', 'v': '5.131', 'random_id': int(time.time() * 100000)}
        async with aiohttp.ClientSession() as __session:
            async with __session.post("https://api.vk.com/method/messages.send", data=params) as resp:
                resp = await resp.json()
        logging.debug(f"------|SEND MESSAGE: {resp.get('response')}|------")


async def mark_as_read(peer_id, mark_conversation_as_read: bool = False):
    params = {'access_token': config.token, 'peer_id': peer_id, 'v': '5.131',
              'mark_conversation_as_read': int(mark_conversation_as_read)}
    async with aiohttp.ClientSession() as __session:
        await __session.get(r"https://api.vk.com/method/messages.markAsRead", params=params)


class ImageUploader(object):
    def __init__(self, peer_id, image):
        self.image = image
        self.params = {"access_token": config.token, "peer_id": peer_id, 'v': '5.131'}

    async def upload(self):
        if isinstance(self.image, bytes):
            photo = await self._upload_image(self.image)
        else:
            async with aiofiles.open(self.image, mode='rb') as data:
                content = await data.read()
            photo = await self._upload_image(content)
        return photo

    async def _upload_image(self, content):
        async with aiohttp.ClientSession() as __session:
            async with __session.get("https://api.vk.com/method/photos.getMessagesUploadServer",
                                     params=self.params) as resp:
                response = await resp.json()
                logging.warning(response)
            upload_url = response['response']['upload_url']
            form_data = aiohttp.FormData()
            form_data.add_field('photo', content, filename='photo.jpg')
            async with __session.post(upload_url, data=form_data) as resp:
                upload = await resp.read()
                response = json.loads(upload)
            params = {"access_token": config.token, "photo": response['photo'],
                      "server": response['server'], 'hash': response['hash'], 'v': '5.131'}
            async with __session.get("https://api.vk.com/method/photos.saveMessagesPhoto", params=params) as resp:
                url_img = await resp.json()
                logging.warning(url_img)
        return f"photo{url_img['response'][0]['owner_id']}_{url_img['response'][0]['id']}"


async def random_picture(redis):
    params = {'access_token': config.user_token,
              'filter': 'owner', 'owner_id': str(config.group_id), 'count': 10,
              'v': '5.131'}
    count_arts = await redis.get("count_arts")
    async with aiohttp.ClientSession() as session:
        if not count_arts:
            async with session.get("https://api.vk.com/method/wall.get", params=params) as resp:
                resp = await resp.json()
                count_arts = resp['response']['count']
                await redis.setex("count_arts", 518400, int(count_arts))
        else:
            offset = random.randint(1, int(count_arts))
            async with session.get(
                    "https://api.vk.com/method/wall.get", params={**params, 'offset': offset}
            ) as resp:
                resp = await resp.json()

    attachments = []
    message = ''
    owner_id, photo_id = '', ''
    for post in resp['response']['items']:
        if post['text'].startswith('#art') and post.get('attachments'):
            logging.debug('Link to post: https://vk.com/wall%s_%s', config.group_id, post['id'])
            attachments = post['attachments']
            message = '\n'.join(post['text'].split('\n')[1:])
            break

    random.shuffle(attachments)
    for attachment in attachments:
        if attachment.get('type', '') == 'photo':
            if attachment['photo']['owner_id'] != config.group_id:
                continue

            photo_id = attachment['photo']['id']
            owner_id = attachment['photo']['owner_id']
            break

    if not attachments or not photo_id or not owner_id:
        await asyncio.sleep(0.3)
        return await random_picture(redis)
    return f"photo{owner_id}_{photo_id}", message


async def random_wall_gif(redis):
    try:
        count_gif = await redis.get(f"count_gif")
        async with aiohttp.ClientSession() as __session:
            if not count_gif:
                params = {
                    'access_token': config.user_token,
                    'owner_id': str(config.group_id), 'count': '1', 'v': '5.131', 'offset': 0, 'type': 3}
                async with __session.get(r"https://api.vk.com/method/docs.get", params=params) as response:
                    doc = await response.json()
                    count_of_gifs = doc['response']['count'] - 1
                await redis.setex(f"count_gif", 518400, int(count_of_gifs))
                await asyncio.sleep(1)
                count_gif = count_of_gifs
            params = {
                'access_token': config.user_token,
                'owner_id': str(config.group_id), 'count': '1', 'v': '5.131',
                'offset': random.randint(0, int(count_gif)), 'type': 3}
            async with __session.get(r"https://api.vk.com/method/docs.get", params=params) as response:
                gif = await response.json()
            attachment = f"doc{gif['response']['items'][0]['owner_id']}_{gif['response']['items'][0]['id']}"
        return "Держи гифочку", attachment
    except Exception as e:
        logging.exception(e)
        return 'Ошибка. Попробуй позже', ''


async def send_custom_keyboard(peer_id, message, keyboard, attachment=""):
    params = {'access_token': config.token, 'peer_id': peer_id, 'message': message, 'attachment': attachment,
              'keyboard': json.dumps(keyboard, ensure_ascii=False), 'v': '5.131',
              'random_id': int(time.time() * 100000)}
    try:
        async with aiohttp.ClientSession() as __session:
            async with __session.post("https://api.vk.com/method/messages.send", data=params) as resp:
                resp = await resp.json()
        logging.debug(f"------|SEND MESSAGE: {resp.get('response')}|------")
    except Exception as e:
        logging.exception(e)
        async with aiohttp.ClientSession() as __session:
            async with __session.post("https://api.vk.com/method/messages.send", data=params) as resp:
                resp = await resp.json()
        logging.debug(f"------|SEND MESSAGE: {resp.get('response')}|------")
