import asyncio
import json
from typing import Optional
from urllib import parse

import aiohttp


class TraceMoeAPI:
    def __init__(self, token: str = None):
        self.api_url = 'https://api.trace.moe'
        self.site_url = 'https://trace.moe'
        self.token = token

        self.anilist_url = 'https://graphql.anilist.co'

    async def is_quota_available(self) -> bool:
        me, _ = await self.about_me()
        return me.get('quota', 1000) != me.get('quotaUsed', 0)

    def get_search_on_site_url(self, image_url: str) -> str:
        return f"{self.site_url}/?url={parse.quote_plus(image_url)}"

    async def about_me(self):
        return await self.request('GET', f"{self.api_url}/me")

    async def search(self, data: bytes) -> Optional[dict]:
        quota_available = await self.is_quota_available()
        if quota_available:
            for attempt in range(15):
                response, status = await self.request('POST', f"{self.api_url}/search", data={'image': data})
                if status == 402:
                    await asyncio.sleep(attempt)
                    continue
                elif status == 200:
                    return response
        return None

    @staticmethod
    async def request(method: str, url: str, json_: dict = None, params=None, data=None) -> (str, int):
        async with aiohttp.request(method, url, params=params, data=data, json=json_) as resp:
            try:
                response = await resp.json()
            except aiohttp.ContentTypeError:
                response = await resp.read()
                response = json.loads(response)
            return response, resp.status

    async def get_data_from_anilist(self, anilist_id):
        query = '''
        query ($id: Int) { # Define which variables will be used in the query (id)
          Media (id: $id, type: ANIME) { # Insert our variables into the query arguments (id) (type: ANIME is hard-coded in the query)
            id
            idMal
            isAdult
            title {
              romaji
              english
              native
            }
          }
        }
        '''

        for attempt in range(5):
            response, status = await self.request('POST', self.anilist_url,
                                                  json_={'query': query, 'variables': {'id': anilist_id}})
            if status != 200:
                await asyncio.sleep(attempt)
                continue
            elif status == 200:
                return response
        return None
