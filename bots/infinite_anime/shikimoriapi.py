import time

import aiohttp
import aiosqlite

from bots.infinite_anime.config import Shikimori
from .message import bot_path, os

DB_PATH = os.path.join(bot_path, "db", "shikimori.sqlite")
domain = "shikimori.one"


async def update_token():
    async with aiosqlite.connect(DB_PATH) as connect:
        async with connect.execute("SELECT access_token, refresh_token, update_at FROM token") as cursor:
            access_token, refresh_token, old_update_at = await cursor.fetchone()
        async with aiohttp.ClientSession() as __session:
            async with __session.post(f"https://{domain}/oauth/token", headers=Shikimori.headers,
                                      params={'grant_type': "refresh_token", 'client_id': Shikimori.client_id,
                                              'client_secret': Shikimori.client_secret,
                                              'refresh_token': refresh_token}) as response:
                response = await response.json()
        await connect.execute("UPDATE token SET update_at = '%s' WHERE update_at = '%s'" %
                              (int(time.time()), old_update_at))
        await connect.execute("UPDATE token SET access_token = '%s' WHERE access_token = '%s'" %
                              (response['access_token'], access_token))
        await connect.execute("UPDATE token SET refresh_token = '%s' WHERE refresh_token = '%s'" %
                              (response['refresh_token'], refresh_token))
        await connect.commit()


async def check_token_expire():
    async with aiosqlite.connect(DB_PATH) as connect:
        async with connect.execute("SELECT update_at FROM token") as cursor:
            ttl = await cursor.fetchone()
            ttl = ttl[0]
    if (int(time.time()) - ttl) > 85200:
        await update_token()


class ShikimoriApi:
    def __init__(self):
        self.headers = Shikimori.headers.copy()
        # self.update_genres()

    async def create_headers(self):
        await check_token_expire()
        async with aiosqlite.connect(DB_PATH) as connect:
            async with connect.execute("SELECT access_token FROM token") as cursor:
                access_token = await cursor.fetchone()
                access_token = access_token[0]
        self.headers['Authorization'] = f'Bearer {access_token}'
        return True

    async def update_genres(self):
        async with aiohttp.ClientSession() as __session:
            async with __session.get(f'https://{domain}/api/genres', headers=self.headers) as response:
                list_genres = await response.json()
        ani_genres = [x for x in list_genres if x['kind'] == 'anime']
        async with aiosqlite.connect(DB_PATH) as connect:
            for i in ani_genres:
                await connect.execute("INSERT INTO genres (id, name_eng, name_rus) VALUES ('%s', '%s', '%s')" % (
                    i['id'], i['name'], i['russian']))
            await connect.commit()

    async def get_info_anime(self, ani_id):
        async with aiohttp.ClientSession() as __session:
            async with __session.get(f'https://{domain}/api/animes/{ani_id}', headers=self.headers) as response:
                anime_info = await response.json()
        return anime_info

    async def get_anime(self, params):
        async with aiohttp.ClientSession() as __session:
            async with __session.get(f'https://{domain}/api/animes', headers=self.headers, params=params) as response:
                anime = await response.json()
        return anime

    async def get_anime_screenshots(self, ani_id):
        pass
