#!/usr/bin/env python3.7
import asyncio
import logging
import sys

import aioredis
from aiohttp import web
from aiojobs.aiohttp import setup

from bots.infinite_anime.message import msg_processor as msg_anime

try:
    port = sys.argv[1]
except:
    port = 5000
logging.basicConfig(level=logging.DEBUG, format=u"%(levelname)-8s[%(asctime)s] | %(message)s",
                    datefmt='%m/%d/%Y %H:%M:%S',
                    filemode='w', filename=f"out{port}.log")
logging.getLogger("aiosqlite").setLevel(logging.CRITICAL)
logging.getLogger("aiogram").setLevel(logging.INFO)
logging.info('| Start Program |')


async def on_startup(app):
    app['redis_1'] = await aioredis.create_redis_pool('redis://localhost', db=1, timeout=100)
    app['redis_2'] = await aioredis.create_redis_pool('redis://localhost', db=2, timeout=100)


async def init_app():
    _app = web.Application(middlewares=[])
    setup(_app)
    _app.on_startup.append(on_startup)
    _app.router.add_post('/anime', msg_anime)
    return _app


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    try:
        app = loop.run_until_complete(init_app())
        web.run_app(app, host='localhost', port=port)
    except Exception as e:
        print('Error create server: %r' % e)
    loop.close()
